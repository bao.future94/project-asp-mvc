﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace project_asp_mvc.Controllers
{
    public class LanguageController : BaseController
    {
        public LanguageController() : base()
        {

        }
        // GET: Language
        public ActionResult SetLanguage(string lang)
        {
            if (lang!=null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(lang);
            }
            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = lang;
            Response.Cookies.Add(cookie);
            Session["lang"] = lang;
            
            return RedirectToAction("Index","Home");
        }
    }
}