﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace project_asp_mvc.Controllers
{
    public class HeaderController : BaseController
    {
        project_asp_mvc.ProjectWebMVCTeam2Entities dbEntities = new ProjectWebMVCTeam2Entities();
        public HeaderController() :base()
        {

        }
        // GET: Header
        public ActionResult Index()
        {  
            return View("_Header");
        }
    }
}