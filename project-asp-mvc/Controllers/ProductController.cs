﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace project_asp_mvc.Controllers
{
    public class ProductController : BaseController
    {
        public ProductController()
        {

        }
        ProjectWebMVCTeam2Entities db = new ProjectWebMVCTeam2Entities();
        // GET: Product
        public ActionResult Index()
        {
            return View("MainContent");
        }
        
        public ActionResult CategoryById(int? id)
        {
            //string CurrentLang = (Session["lang"] != null) ? Session["lang"] as string : LanguageDefault;

            //List<Category> ListCategoiesParent = db.Categories.Where(c => c.language_id.Equals(CurrentLang) && c.parent_id == 0).ToList();
            //List<Category> ListSubCategories = new List<Category>();
            //List<Product> ListProducts = db.Products.Where(p => p.language_id.Equals(CurrentLang)).OrderByDescending(p => p.id).ToList();
            //Dictionary<int, List<int>> SubCategories = new Dictionary<int, List<int>>();
            //foreach (var catParent in ListCategoiesParent)
            //{
            //    var ListIdSubCat = new List<int>();
            //    foreach (var subCat in ListSubCategories)
            //    {
            //        if (catParent.id == subCat.parent_id)
            //        {
            //            ListIdSubCat.Add(subCat.id);
            //        }
            //    }
            //    SubCategories.Add(catParent.id, ListIdSubCat);
            //}

            //ViewBag.ListCategoiesParent = ListCategoiesParent;
            //ViewBag.ListProducts = ListProducts;
            //ViewBag.SubCategories = SubCategories;
            string CurrentLang = (Session["lang"] != null) ? Session["lang"] as string : LanguageDefault;
            ViewBag.CurrentLang = CurrentLang;
            List<Product> ListProductSlides = db.Products.Where(p => p.language_id.Equals(CurrentLang)).OrderByDescending(p => p.id).Take(4).ToList();
            ViewBag.ListProductSlides = ListProductSlides;

            ViewBag.RelatedProduct = db.Products.Where(p => p.category_id == id && p.language_id.Equals(CurrentLang)).ToList();
            return View("Category");
        }
    }
}