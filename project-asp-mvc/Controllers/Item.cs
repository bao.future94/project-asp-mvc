﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_asp_mvc.Controllers
{
    public class Item
    {
        private Product prod = new Product();
        private int quantity;
        
        public Product Prod { get => prod; set => prod = value; }
        public int Quantity { get => quantity; set => quantity = value; }

        public Item()
        {

        }
        public Item(Product product, int quantity)
        {
            this.prod = product;
            this.quantity = quantity;
        }
    }
}