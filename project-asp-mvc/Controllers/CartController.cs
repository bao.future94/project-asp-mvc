﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace project_asp_mvc.Controllers
{
    public class CartController : BaseController
    {
        public CartController() :base()
        {

        }
        ProjectWebMVCTeam2Entities db = new ProjectWebMVCTeam2Entities();
        // GET: Cart
        public ActionResult Index()
        {
            string CurrentLang = (Session["lang"] != null) ? Session["lang"] as string : LanguageDefault;
            ViewBag.CurrentLang = CurrentLang;
            List<Product> ListProductSlides = db.Products.Where(p => p.language_id.Equals(CurrentLang)).OrderByDescending(p => p.id).Take(4).ToList();
            ViewBag.ListProductSlides = ListProductSlides;

            return View("Cart");
        }
        // Check Existing Item
        private int IsExisting(int Id)
        {
            List<Item> cart = (List<Item>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
                if (cart[i].Prod.id == Id)
                    return i;
            return -1;
        }
        // Create Session
        public ActionResult AddToCart(int Id)
        {
            string CurrentLang = (Session["lang"] != null) ? Session["lang"] as string : LanguageDefault;
            ViewBag.CurrentLang = CurrentLang;
            List<Product> ListProductSlides = db.Products.Where(p => p.language_id.Equals(CurrentLang)).OrderByDescending(p => p.id).Take(4).ToList();
            ViewBag.ListProductSlides = ListProductSlides;
            if (Session["cart"] == null)
            {
                List<Item> cart = new List<Item>
                {
                    new Item(db.Products.Find(Id), 1)
                };
                Session["cart"] = cart;
            }
            else
            {
                List<Item> cart = (List<Item>)Session["cart"];
                int index = IsExisting(Id);
                if (index == -1)
                    cart.Add(new Item(db.Products.Find(Id), 1));
                else
                    cart[index].Quantity++;
                Session["cart"] = cart;
            }
            return View("Cart");
        }

        //Update item in cart
        public ActionResult Update(FormCollection fc)
        {
            string[] quanlities = fc.GetValues("quantity");
            List<Item> cart = (List<Item>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
                cart[i].Quantity = Convert.ToInt32(quanlities[i]);
            Session["cart"] = cart;
            return View("Cart");
        }
        // Delete item in cart
        public ActionResult Delete(int Id)
        {
            string CurrentLang = (Session["lang"] != null) ? Session["lang"] as string : LanguageDefault;
            List<Product> ListProductSlides = db.Products.Where(p => p.language_id.Equals(CurrentLang)).OrderByDescending(p => p.id).Take(4).ToList();
            ViewBag.ListProductSlides = ListProductSlides;
            int index = IsExisting(Id);
            List<Item> cart = (List<Item>)Session["cart"];
            cart.RemoveAt(index);
            Session["cart"] = cart;

            return View("Cart");
        }
        // Check user login to checkout
        public ActionResult Checkout(FormCollection fc)
        {
            string CurrentLang = (Session["lang"] != null) ? Session["lang"] as string : LanguageDefault;
            List<Product> ListProductSlides = db.Products.Where(p => p.language_id.Equals(CurrentLang)).OrderByDescending(p => p.id).Take(4).ToList();
            ViewBag.ListProductSlides = ListProductSlides;
            if (Session["username"] != null)
            {
                return View("Checkout");
            }
            else
            {
                return RedirectToAction("Index","User");
            }
        }
        //Checkout
        public ActionResult SaveOrder(FormCollection fc)
        {
            string CurrentLang = (Session["lang"] != null) ? Session["lang"] as string : LanguageDefault;
            List<Product> ListProductSlides = db.Products.Where(p => p.language_id.Equals(CurrentLang)).OrderByDescending(p => p.id).Take(4).ToList();
            ViewBag.ListProductSlides = ListProductSlides;
            List<Item> cart = (List<Item>)Session["cart"];
            string lang = Session["username"] as string;
            Account account = db.Accounts.SingleOrDefault(a => a.username.Equals(lang));
            //save Invoice
            Order order = new Order();
            order.createdate = DateTime.Now;
            order.Account = account;
            order.customer_name = fc["customer_name"];
            order.address = fc["address"];
            db.Orders.Add(order);
            db.SaveChanges();

            //save Invoice Detail
            
            foreach (Item Item in cart)
            {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.order_id = order.id;
                orderDetail.product_id = Item.Prod.id;
                orderDetail.quantity = Item.Quantity;
                db.OrderDetails.Add(orderDetail);
                db.SaveChanges();
            }

            //Clear all item in cart
            Session.Remove("cart");

            return View("Thanks");
        }

    }
}