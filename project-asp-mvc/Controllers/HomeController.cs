﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using project_asp_mvc.Models;

namespace project_asp_mvc.Controllers
{
    public class HomeController : BaseController
    {
        ProjectWebMVCTeam2Entities db = new ProjectWebMVCTeam2Entities();

        public HomeController() : base()
        {

        }

        public ActionResult Index()
        {
            
            string CurrentLang = (Session["lang"] != null) ? Session["lang"] as string : LanguageDefault;
            List<Category> ListSubCategories = new List<Category>();
            
            Dictionary<int, List<int>> SubCategories = new Dictionary<int, List<int>>();
            ListSubCategories.AddRange(db.Categories.Where(c => c.parent_id != 0).ToList());
            
            List<Category> ListCategoiesParent = db.Categories.Where(c => c.language_id.Equals(CurrentLang) && c.parent_id == 0).ToList();
            List<Product> ListProducts = db.Products.Where(p => p.language_id.Equals(CurrentLang)).OrderByDescending(p => p.id).ToList();
            foreach (var catParent in ListCategoiesParent)
            {
                var ListIdSubCat = new List<int>();
                foreach (var subCat in ListSubCategories)
                {
                    if (catParent.id == subCat.parent_id)
                    {
                        ListIdSubCat.Add(subCat.id);
                    }
                }
                SubCategories.Add(catParent.id, ListIdSubCat);
            }
            
           // ViewBag.ListCategoiesParent = ListCategoiesParent;
            //ViewBag.ListProducts = ListProducts;
            ViewBag.SubCategories = SubCategories;
            ViewBag.CurrentLang = CurrentLang;
            ViewBag.TopNew = this.db.Products.Where(p => p.language_id.Equals(CurrentLang)).OrderByDescending(p => p.id).Take(3).ToList();
            ViewBag.TopSeller = this.db.Products.Where(p => p.language_id.Equals(CurrentLang) && p.OrderDetails.Count() >= 100).OrderByDescending(p => p.OrderDetails.Count()).Take(3).ToList();
            ViewData["RecentProductList"] = Session["RecentProductList"];
            ViewBag.ListProductSlides = db.Products.Where(p=>p.language_id.Equals(CurrentLang)).OrderByDescending(p=>p.id).Take(4).ToList();

            //var productModel = new ProductModel();
            //productModel.ListProducts = ListProducts;
            //Thu nha
            List<Category> ListSubCategoiesHeader = new List<Category>();
            foreach( var catParent in ListCategoiesParent)
            {
               foreach (var sub in db.Categories.Where(c => c.parent_id == catParent.id && c.language_id.Equals(CurrentLang)).ToList())
                {
                    ListSubCategoiesHeader.Add(sub);
                }
            }
            HomeModel homeModel = new HomeModel();
            homeModel.ListCategoiesParent = ListCategoiesParent;
            homeModel.ListProducts = ListProducts;
            homeModel.ListSubCategoiesHeader = ListSubCategoiesHeader;

            return View("MainContent", homeModel);
        }

        public ActionResult Detail(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Product product = db.Products.Find(id);

            if (product == null)
                return HttpNotFound();

            var list = Session["RecentProductList"] as List<Product>;

            if (list == null)
            {
                // If list not found in session, create list and store it in a session
                list = new List<Product>();
                Session["RecentProductList"] = list;
            }

            // Add product to recent list (make list contain max 10 items; change if you like)
            Utils.Utils.AddRecentProduct(list, product, 3);
            ViewData["RecentProductList"] = list;

            if (id != null)
            {
                var products = db.Products.Find(id);
                if (products != null)
                {
                    string CurrentLang = (Session["lang"] != null) ? Session["lang"] as string : LanguageDefault;

                    ViewBag.product = db.Products.Where(p => p.id == id).SingleOrDefault();
                    var cate_id = db.Products.Where(p => p.id == id).Select(b => b.category_id).SingleOrDefault();
                    ViewBag.RelatedProduct = db.Products.Where(p => p.category_id == cate_id).ToList();

                    //ViewBag.CurrentLang = CurrentLang;
                    ViewBag.ResultSet = Session["SearchResult"];
                    return View(products);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        public ActionResult Search(int id, String SearchString)
        {
            List<Product> ResultSet = db.Products.Where(p => p.name.Contains(SearchString) || p.description.Contains(SearchString)).ToList();
            ViewBag.ResultSet = ResultSet;
            Session["SearchResult"] = ResultSet;
            return RedirectToAction("Detail", new { Id = id });
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [HttpPost]
        public ActionResult Contact(Email email,FormCollection form)
        {
            var name = form["Name"];
            var phone = form["Phone"];
            
            MailMessage mailMessage = new MailMessage("ngtuananh1995@gmail.com", email.To);
            mailMessage.Subject = email.Subject;
            mailMessage.Body = "[Name]:\t" +name + "\n" + "[Phone]:" +phone + "\n" + email.Body;
            mailMessage.IsBodyHtml = false;

            SmtpClient smtp = new SmtpClient();
            NetworkCredential nwCredential = new NetworkCredential("ngtuananh1995@gmail.com", "choancut");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = nwCredential;
            smtp.EnableSsl = true;
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.EnableSsl = true;

            smtp.Send(mailMessage);

            smtp.Dispose();
            return View();
        }
    }
}