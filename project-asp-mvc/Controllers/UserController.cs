﻿using project_asp_mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace project_asp_mvc.Controllers
{
    public class UserController : Controller
    {
        UserDAO userDAO = new UserDAO();

        // GET: User
        public ActionResult Index()
        {
            return View("Login");
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Account user)
        {
            Account account = userDAO.IsValidAccount(user.username, user.password);

            if (account != null)
            {
                // storing in cookie
                Session["username"] = user.username;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Username or password is invalid");
            }
            return View();

        }
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Account account)
        {
            userDAO.Register(account);
            return RedirectToAction("Index", "Home");
        }
    }

    public class UserDAO : Controller
    {
        private ProjectWebMVCTeam2Entities StoreContext = null;

        public UserDAO()
        {
            StoreContext = new ProjectWebMVCTeam2Entities();
        }

        public int ValidateAccount(Account user)
        {
            int result = 1;

            if (String.IsNullOrEmpty(user.password))
                result = -1;
            else if (String.IsNullOrEmpty(user.ConfirmPassword))
                result = -2;
            else if (!Utils.Utils.Encode(user.password).Equals(Utils.Utils.Encode(user.ConfirmPassword)))
                result = -3;
            else if (this.StoreContext.Accounts.Find(user.username) != null)
                result = -4;

            return result;
        }

        public void Register(Account user)
        {
            // encoding password
            user.password = user.ConfirmPassword = Utils.Utils.Encode(user.password);

            // setting role for account to "USER" type
            user.role_id = 2;
            user.Role = this.StoreContext.Roles.Find(user.role_id);

            // save change to db
            StoreContext.Accounts.Add(user);
            StoreContext.SaveChanges();
            ModelState.Clear();
        }
        
        public Account IsValidAccount(String username, string password)
        {
            String encoded = Utils.Utils.Encode(password);
            return this.StoreContext.Accounts.Where(a => a.username.Equals(username) && a.password.Equals(encoded))
                .SingleOrDefault();
        }
    }
}