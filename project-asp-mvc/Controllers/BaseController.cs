﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace project_asp_mvc.Controllers
{
    public class BaseController : Controller
    {
        ProjectWebMVCTeam2Entities db = new ProjectWebMVCTeam2Entities();
        protected string LanguageDefault = "";
        public BaseController()
        {

            Language language = db.Languages.Single(l => l.language_default == 1);
            LanguageDefault = language.id;

        }

    }
}