﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace project_asp_mvc.Models
{
    public class Email
    {
        [Required]
        [EmailAddress]
        public string To { get; set; }

        public string From { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Body { get; set; }
    }
}