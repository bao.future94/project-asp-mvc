﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_asp_mvc.Models
{
    public class HomeModel
    {
        public List<Category> ListCategoiesParent { get; set; }
        public List<Category> ListSubCategoiesHeader { get; set; }
        public List<Product> ListProducts { get; set; }
    }
}