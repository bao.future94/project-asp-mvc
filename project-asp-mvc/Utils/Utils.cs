﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_asp_mvc.Controllers.Utils
{
    public class Utils
    {
        public static string Encode(string value)
        {
            var hash = System.Security.Cryptography.SHA1.Create();
            var encoder = new System.Text.ASCIIEncoding();
            var combined = encoder.GetBytes(value ?? "");
            return BitConverter.ToString(hash.ComputeHash(combined)).ToLower().Replace("-", "");
        }

        public static void AddRecentProduct(List<Product> list, Product product, int maxItems)
        {
            // list is current list of RecentProduct objects
            // Check if item already exists
            var item = list.FirstOrDefault(t => t.id == product.id);
            // TODO: here if item is found, you could do some more coding
            //       to move item to the end of the list, since this is the
            //       last product referenced.

            if (item == null)
            {
                // Add item only if it does not exist
                list.Add(product);
            }


            // Check that recent product list does not exceed maxItems
            // (items at the top of the list are removed on FIFO basis;
            // first in, first out).
            while (list.Count > maxItems)
            {
                list.RemoveAt(0);
            }
        }
    }
}